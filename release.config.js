const fs = require("fs");
const path = require("path");

const parserOptions = {
  mergePattern: /([A-Z0-9-/]+)\/(\w+)\.\s?(.*)/,
  issuePrefixes: "HBA-",
  mergeCorrespondence: ["id", "type", "subject"],
};

module.exports = {
  // eslint-disable-next-line no-template-curly-in-string
  tagFormat: "${version}",
  branches: ["master"],
  plugins: [
    [
      "@semantic-release/commit-analyzer",
      {
        preset: "conventionalcommits",
        parserOpts: parserOptions,
        releaseRules: [
          { type: "feat", release: "minor" },
          { type: "feature", release: "minor" },
          { type: "fix", release: "patch" },
          { type: "release", release: "major" },
        ],
      },
    ],

    [
      "@semantic-release/release-notes-generator",
      {
        preset: "conventionalcommits",
        parserOpts: parserOptions,
        writerOpts: {
          commitsSort: ["id"],
          commitPartial: fs.readFileSync(
            path.join(__dirname, "flow", "changelogTemplates", "commit.hbs"),
            "utf-8"
          ),
        },
      },
    ],

    "@semantic-release/npm",

    [
      "@semantic-release/gitlab",
      {
        successComment: false,
      },
    ],

    [
      "@semantic-release/git",
      {
        assets: ["package.json"],
        message:
          // eslint-disable-next-line no-template-curly-in-string
          "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}",
      },
    ],
  ],
};
